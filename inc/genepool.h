#if !defined(GENEPOOL_H)
# define GENEPOOL_H
# include <stddef.h>
# include "brain.h"
# include "map.h"
# include "simulate.h"

typedef struct genepool genepool_t;
typedef struct genepool_job genepool_job_t;

#define MAX_GENEPOOL_SIZE 250

struct genepool
{
    int  size;
    brain_t members[MAX_GENEPOOL_SIZE];
    double  totalscore[MAX_GENEPOOL_SIZE];
    brain_t best;
    double  bestscore;
};

struct genepool_job
{
    genepool_t *gp;
    map_t      *maps;
    size_t      nmaps;
    pthread_t   thread;
};

void genepool_job_start(genepool_job_t *job, genepool_t *gp, map_t *maps, size_t nmaps);

void genepool_job_wait(genepool_job_t *job);

void genepool_interbreed(genepool_t *gp);

void genepool_calculate_scores(genepool_t *gp, map_t *maps, size_t nmaps);

void genepool_split(genepool_t *gp, genepool_t *oth);

double genepool_avg_score(genepool_t *gp);
double genepool_max_score(genepool_t *gp);

void genepool_choose_best(genepool_t *gp);

void genepools_rebalance(genepool_t *gpvec, size_t n);

#endif
