#if !defined(SIMULATE_H)
# define SIMULATE_H

# include <stdbool.h>
# include <stddef.h>

# define MAX_CARS 16
# define CAR_WIDTH 14
# define CAR_HEIGHT 1

typedef struct simulation simulation_t;
typedef struct car        car_t;

typedef enum
{
    DIR_UP = 0,
    DIR_RIGHT = 1,
    DIR_DOWN = 2,
    DIR_LEFT = 3,
} car_dir_t;

# include "brain.h"
# include "map.h"

struct car
{
    int  waypointind;
    int  waypoints[MAX_WAYPOINTS][2];
    int  numwaypoints;
    bool extant;
    bool crashed;
    brain_t brain;
    double x, y;
    double dir;
    int id;
    int steps;
    double tx, rx;
};

struct simulation
{
    bool  ended;
    car_t cars[MAX_CARS];
    map_t *map;
};

/*
 * BRAIN DESIGN
 *
 * -- INPUTS ----------
 * Node 0  - Const 1
 * Node 1  - Random bit (1 or 0)
 * Node 2  - Car ID (0.0 to 1.0)
 * Node 3  - Flash Visible (0.0 to 1.0, diminishing with distance)
 * Node 4  - X
 * Node 5  - X
 * Node 6  - X
 * Node 7  - X
 * Node 8  - X
 * Node 9  - X
 * Node 10 - X
 * Node 11 - X
 * Node 12 - X
 * Node 13 - X
 * Node 14 - X
 * Node 15 - X
 *
 * -- HIDDEN LAYER ---
 * Nodes 16-23
 *
 * -- OUTPUTS ---
 * Node 24 - Drive (1 => Go, 0 => Stop)
 * Node 25 - Turn (-1 => left, 0 => forward, 1 => right)
 * Node 26 - Flash Lights (1 => Flash, 0 => No flash)
 * Node 27 - X
 * Node 28 - X
 * Node 29 - X
 * Node 30 - X
 * Node 31 - X
 *
 */


typedef enum
{
    NODE_CONST_1   = 0,
    NODE_RAND      = 1,
    NODE_ID        = 2,
    NODE_SEE_FLASH = 3,
    NODE_GRASS_LEFT = 4,
    NODE_GRASS_RIGHT = 5,
    NODE_GRASS_FRONT = 6,
    NODE_WAYPOINT_ANGLE = 7,
    NODE_RX = 8,
    NODE_LAST_INPUT = 8,

    NODE_FIRST_HIDDEN = 16,
    NODE_LAST_HIDDEN = 23,

    NODE_FIRST_OUTPUT = 24,

    NODE_DRIVE     = 24,
    NODE_TURN      = 25,
    NODE_DO_FLASH  = 26,

    NODE_LAST_OUTPUT
    
} node_purpose_t;

void simulation_init(simulation_t *sim, map_t *map, brain_t *brain);

void simulation_step(simulation_t *sim);

double simulation_score(simulation_t *sim);

void car_think(car_t *car, simulation_t *sim);

void car_drive(car_t *car, simulation_t *sim);

void car_init(car_t *car, brain_t *brain);


double car_score(car_t *car);

#endif
