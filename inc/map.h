#if !defined(MAP_H)
# define MAP_H

#include<stdbool.h>
#include<stdlib.h>
#include<stdio.h>
#include<time.h>
#include<string.h>

#define MAP_HEIGHT 16 // 16 tiles
#define MAP_WIDTH 16  // 16 tiles
#define TILE_SIZE 6 // 6x6
#define MAX_WAYPOINTS 32

#define ROUTE_MAX_STEPS (MAP_HEIGHT * MAP_WIDTH)

#define WOBBLE_0 20
#define WOBBLE_1 5

typedef struct map map_t;

#include"simulate.h"

typedef int cars_tile_t[MAX_CARS][2];
typedef int    tile_t[2];
typedef double coord_t[2];

typedef  tile_t route_t[ROUTE_MAX_STEPS];
typedef coord_t waypoints_t[MAX_WAYPOINTS];

struct map
{
    int  num_cars;
    bool road[MAP_WIDTH][MAP_HEIGHT];

    // Car start position in _coordinates_
    coord_t start_pos[MAX_CARS];

    // Car start direction in degrees
    double start_dir[MAX_CARS];

    // Waypoints, for each car, in _coordinates_
    waypoints_t waypoints[MAX_CARS];

    // Number of waypoints for each car
    int numwaypoints[MAX_CARS];
};


void tileToCoord(tile_t tile, coord_t coord);
bool routeStepRight(route_t route, size_t* step);
bool routeStepLeft(route_t route, size_t* step);
bool routeStepDown(route_t route, size_t* step);
bool routeStepUp(route_t route, size_t* step);

void makeRoute(
    tile_t start_tile,
    car_dir_t direction,
    route_t route,
    waypoints_t waypoints,
    int* num_steps,
    int* numwaypoints
);

bool tileTaken(
    tile_t      tile_to_check,
    cars_tile_t other_tiles,
    int         check_till // index to stop before
);

void generateStartTiles(
    int         num_cars,
    cars_tile_t start_tiles,
    car_dir_t direction[MAX_CARS]
);

void printRoute(route_t route, int num_steps);
void buildRoads(
    route_t routes[MAX_CARS],
    int num_steps[MAX_CARS],
    int num_cars,
    bool road[MAP_WIDTH][MAP_HEIGHT]
);

void printRoads(bool road[MAP_WIDTH][MAP_HEIGHT]);

map_t generateMap();
void printMap(map_t* map);

#endif
