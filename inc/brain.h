#if !defined(BRAIN_H)
# define BRAIN_H
# include <stddef.h>
# include <stdbool.h>
# include <stdlib.h>

typedef struct brain  brain_t;
typedef struct node   node_t;
typedef struct neuron neuron_t;

# define MAX_NEURONS 64
# define MAX_NODES   64
# define MAX_INPUTS  64

struct neuron
{
    bool    extant;
    double  val;
    size_t  inpnode;
};

struct node
{
    bool      extant;
    double    val;
    bool      output;
    bool      input;
    bool      calculated;
    neuron_t  links[MAX_INPUTS];
};

struct brain
{
    node_t   nodes[MAX_NODES];
};

static inline double rand_float(void)
{
    return ((double)rand())/((double)RAND_MAX);
}

void brain_init(brain_t *brain);

void brain_copy(brain_t *brain, brain_t *oth);

void brain_calculate_outputs(brain_t *brain);

void brain_calculate_node(brain_t *brain, node_t *node);

void brain_clear_calculated(brain_t *brain);

void brain_mutate(brain_t *brain);

#endif
