import lib
import ctypes
import time
import os

print("Seeding RNG ...")
lib.srand(int(time.time()*1000))

print("Generating a brain ...")
brain = lib.brain_t()
lib.brain_init(ctypes.byref(brain))

brain.nodes[24].disconnect()
brain.nodes[24].connect(16)

#brain.nodes[25].disconnect()

brain.nodes[16].disconnect()
brain.nodes[16].connect(0)

print("Generating a map ...")
map   = lib.map_t()

for y in range(lib.MAP_HEIGHT):
    for x in range(3):
        map.road[x][y] = True

map.num_cars = 1
map.start_pos[0][0] = 3
map.start_pos[0][1] = 0
map.start_dir[0]    = 180

map.numwaypoints[0] = 2

map.waypoints[0][0][0] = 3
map.waypoints[0][0][1] = 3

map.waypoints[0][1][0] = 3
map.waypoints[0][1][1] = 27

print("Making a simulation ...")
sim = lib.simulation_t()
lib.simulation_init(ctypes.byref(sim), ctypes.byref(map), ctypes.byref(brain))
print("Recording the result ...")

import json
open("brain-0.json", "w").write(json.dumps(sim.record()))
open("map.json", "w").write(json.dumps(map.serialize()))
