import lib
import json
import ctypes
import time
import os
import sys
import shutil

#---------------------------------------------------------------------------
# Constants
#---------------------------------------------------------------------------
#
PYTHON_PATH = os.path.dirname(os.path.realpath(__file__))
ROOT_PATH   = os.path.dirname(PYTHON_PATH)

OUTPUT_PATH      = os.path.join(PYTHON_PATH, 'output_data', 'current')
PREV_OUTPUT_PATH = os.path.join(PYTHON_PATH, 'output_data', 'prev')

NUM_CARS = 3
NUM_MAPS = 10
NUM_SPECIES = 5
NUM_IN_GENEPOOL = 30

#---------------------------------------------------------------------------
# Functions
#---------------------------------------------------------------------------
#
def generateKnownMap():
    print("Generating a map ...")
    map   = lib.map_t()

    for y in range(lib.MAP_HEIGHT):
        for x in range(lib.MAP_WIDTH):
            if (x, y) in [
                    (0, 0), (0, 1), (0, 2), (0, 3), (0, 4),
                    (1, 4), (2, 4), (3, 4), (4, 4),
                    (4, 5), (4, 6), (4, 7), (4, 8),
                ]:
                map.road[x][y] = True
    map.num_cars = 2
    map.start_pos[0][0] = 3
    map.start_pos[0][1] = 1
    map.start_dir[0]    = 180
    map.start_pos[1][0] = 3
    map.start_pos[1][1] = 10
    map.start_dir[1]    = 180

    map.numwaypoints[0] = 2
    map.waypoints[0][0][0] = 3
    map.waypoints[0][0][1] = 3
    map.waypoints[0][1][0] = 27
    map.waypoints[0][1][1] = 51

    map.numwaypoints[1] = 2
    map.waypoints[1][0][0] = 3
    map.waypoints[1][0][1] = 10
    map.waypoints[1][1][0] = 27
    map.waypoints[1][1][1] = 51


def outputMaps(mapset):
    for i in range(0, 3):
        path = os.path.join(OUTPUT_PATH, f'map-{i}.json')
        open(path, "w").write(json.dumps(mapset[i].serialize()))

def record(generation, n, mapset, brain):
    gen_path = os.path.join(OUTPUT_PATH, f'gen-{generation}')

    if not os.path.exists(gen_path):
        os.mkdir(gen_path)

    sim = lib.simulation_t()
    for i in range(0, 3):
        lib.simulation_init(ctypes.byref(sim), ctypes.byref(mapset[i]), ctypes.byref(brain))
        genepool_path = os.path.join(
            gen_path,
            f'genepool-{n}_map-{i}.json'
        )
        open(genepool_path, "w").write(json.dumps(sim.record(nframes=500)))

def prepareOutputPath():
    if os.path.exists(OUTPUT_PATH):
        if os.path.exists(PREV_OUTPUT_PATH):
            shutil.rmtree(PREV_OUTPUT_PATH)
        os.rename(OUTPUT_PATH, PREV_OUTPUT_PATH)
    os.makedirs(OUTPUT_PATH)

#---------------------------------------------------------------------------
# Preamble
#---------------------------------------------------------------------------
#
if __name__ == '__main__':
    print("Seeding RNG ...")
    lib.srand(int(time.time()*1000))

    print("Generating a brain ...")
    brain = lib.brain_t()
    lib.brain_init(ctypes.byref(brain))

    prepareOutputPath()

def get_mapset():
    mapset = (lib.map_t * NUM_MAPS)()

    for n in range(NUM_MAPS):
        mapset[n] = lib.generateMap(NUM_CARS)

    return mapset

#---------------------------------------------------------------------------
# Main
#---------------------------------------------------------------------------
#
if __name__ == '__main__':
    print("Let's go!")

    species = (NUM_SPECIES * lib.genepool_t)()

    for s in species:
        s.size = NUM_IN_GENEPOOL
        lib.brain_init(ctypes.byref(s.best))

    recordmaps = get_mapset()
    outputMaps(recordmaps)

    generation = 0
    while True:
        mapset = get_mapset()

        generation += 1
        avgs = []
        maxs = []

        jobs = [lib.genepool_job_t() for s in species]
        for j, gp in zip(jobs, species):
            lib.genepool_job_start(j, ctypes.byref(gp), mapset, NUM_MAPS)
        for j in jobs:
            lib.genepool_job_wait(j)

        for n, gp in enumerate(species):
            avgs.append(lib.genepool_avg_score(ctypes.byref(gp)))
            maxs.append(lib.genepool_max_score(ctypes.byref(gp)))

            if generation % 5 == 0:
                record(generation, n, recordmaps, gp.best)

        lib.genepools_rebalance(species, NUM_SPECIES)
        print(
            "Gen: {:3d}  Avg: ".format(generation) + " ".join("{:7.2f}".format(a) for a in avgs) + "  " +
            "Gen: {:3d}  Max: ".format(generation) + " ".join("{:7.2f}".format(m) for m in maxs)
        )
        sys.stdout.flush()

    gen = 0
    while True:
        gen += 1
        brain = iterate_brain([map], brain)
        sim = lib.simulation_t()
        lib.simulation_init(ctypes.byref(sim), ctypes.byref(map), ctypes.byref(brain))
