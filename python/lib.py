from ctypes import *
import os

MAX_GENEPOOL_SIZE = 250

MAX_WAYPOINTS = 32
MAP_HEIGHT = 16
MAP_WIDTH  = 16

MAX_NEURONS = 64
MAX_NODES   = 64
MAX_INPUTS  = 64

MAX_CARS = 16

pythonpath = os.path.dirname(os.path.realpath(__file__))
rootpath   = os.path.dirname(pythonpath)

lib = CDLL(os.path.join(rootpath, "bin/autogta.so"))

class neuron_t(Structure):
    _fields_ = [
        ("extant",  c_bool),
        ("val",     c_double),
        ("inpnode", c_size_t)
    ]

    def __str__(self):
        if not self.extant:
            raise Exception("Neuron does not exist")

        return f"{self.inpnode} -[*{self.val:4.2f}]->"

class node_t(Structure):
    _fields_ = [
        ("extant",     c_bool),
        ("val",        c_double),
        ("output",     c_bool),
        ("input",     c_bool),
        ("calculated", c_bool),
        ("links",      neuron_t * MAX_INPUTS)
    ]

    def __str__(self):
        if not self.extant:
            raise Exception("Node does not exist")
        desc = f"[{self.val}]{' output' if self.output else ''}{' input' if self.input else ''}"

        lines = [str(l) for l in self.links if l.extant]
        if len(lines) > 0:
            lines[int(len(lines) / 2)] += desc
        else:
            lines = [desc]

        return "\n".join(lines)

    def connect(self, n):
        for neuron in self.links:
            if neuron.extant and neuron.inpnode == n:
                neuron.val = 1.0

    def disconnect(self):
        for neuron in self.links:
            if neuron.extant:
                neuron.val = 0.0
class brain_t(Structure):
    _fields_ = [
        ("nodes", node_t * MAX_NODES)
    ]

    def __str__(self):
        lines = []
        for ind in range(MAX_NODES):
            if self.nodes[ind].extant:
                lines += [
                    f"Node {ind}",
                    "---------------"
                ]
                lines += [str(self.nodes[ind])]

        return "\n".join(lines)

class car_t(Structure):
    _fields_ = [
        ("waypointind", c_int),
        ("waypoints",   (c_int * 2) * MAX_WAYPOINTS),
        ("numwaypoints", c_int),
        ("extant",  c_bool),
        ("crashed", c_bool),
        ("brain",   brain_t),
        ("x",       c_double),
        ("y",       c_double),
        ("dir",     c_double),
        ("id",      c_int),
        ("steps",   c_int),
        ("tx",      c_double),
        ("rx",      c_double)
    ]

    def __str__(self):
        if not self.extant:
            raise Exception("Car does not exist")

        return (
            f"<Car {self.id}:"
            f" ({self.x},{self.y}),"
            f" {self.dir} degrees"
            f"{', CRASHED' if self.crashed else ''}>"
        )

    def serialize(self):
        waypoints = [[w[0], w[1]] for w in self.waypoints][:self.numwaypoints]
        if self.waypointind < 0:
            completewp = waypoints
            uncompletewp = []
        else:
            completewp   = waypoints[:self.waypointind]
            uncompletewp = waypoints[self.waypointind:]

        return {
            "x": self.x,
            "y": self.y,
            "r": self.dir,
            "brain_inputs": [n.val for n in self.brain.nodes if n.input],
            "score": self.score(),
            "waypointind": self.waypointind,
            "id": self.id,
            "completed_waypoints": completewp,
            "uncompleted_waypoints": uncompletewp,
            "crashed": self.crashed
        }

    def score(self):
        return car_score(byref(self))

coord_t     = c_double * 2
waypoints_t = coord_t * MAX_WAYPOINTS

class map_t(Structure):
    _fields_ = [
        ("num_cars", c_int),
        ("road", (c_bool * MAP_HEIGHT) * MAP_WIDTH),
        ("start_pos", coord_t * MAX_CARS),
        ("start_dir", c_double * MAX_CARS),
        ("waypoints", waypoints_t * MAX_CARS),
        ("numwaypoints", c_int * MAX_CARS)
    ]

    def serialize(self):
        tiles = [[1 if item else 0 for item in array] for array in self.road]
        return { "size": [ MAP_WIDTH, MAP_HEIGHT ], "tiles": tiles }

class simulation_t(Structure):
    _fields_ = [
        ("ended", c_bool),
        ("cars", car_t * MAX_CARS),
        ("map",  POINTER(map_t))
    ]

    def __str__(self):
        lines = [
            "Simulation",
            "----------"
        ]

        lines += [str(c) for c in self.cars if c.extant]

        return "\n".join(lines)

    def serialize(self):
        return [
            car.serialize() for car in self.cars if car.extant
        ]

    def step(self):
        simulation_step(byref(self))

    def record(self, nframes=30):
        frames = []
        for i in range(nframes):
            frames.append(self.serialize())
            self.step()

        frames.append(self.serialize())
        return {
            "number_of_cars": sum(1 for car in self.cars if car.extant),
            "timestamps": frames
        }

    def score(self):
        return simulation_score(byref(self))

class genepool_t(Structure):
    _fields_ = [
        ("size", c_int),
        ("members", brain_t * MAX_GENEPOOL_SIZE),
        ("totalscore", c_double * MAX_GENEPOOL_SIZE),
        ("best", brain_t),
        ("bestscore", c_double)

    ]

class genepool_job_t(Structure):
    _fields_ = [
        ("gp", POINTER(genepool_t)),
        ("maps", POINTER(map_t)),
        ("nmaps", c_size_t),
        ("thread", c_int)
    ]

simulation_init = lib.simulation_init
simulation_init.argtypes = [POINTER(simulation_t), POINTER(map_t), POINTER(brain_t)]

simulation_step = lib.simulation_step
simulation_step.argtypes = [POINTER(simulation_t)]

brain_init = lib.brain_init
brain_init.argtypes = [POINTER(brain_t)]

brain_copy = lib.brain_copy
brain_copy.argtypes = [POINTER(brain_t), POINTER(brain_t)]

brain_mutate = lib.brain_mutate
brain_mutate.argtypes = [POINTER(brain_t)]

srand = lib.srand
srand.argtypes = [c_uint]

simulation_score = lib.simulation_score
simulation_score.argtypes = [POINTER(simulation_t)]
simulation_score.restype  = c_double

car_score = lib.car_score
car_score.argtypes = [POINTER(car_t)]
car_score.restype  = c_double

genepool_choose_best = lib.genepool_choose_best
genepool_choose_best.argtypes = [POINTER(genepool_t)]

genepool_interbreed = lib.genepool_interbreed
genepool_interbreed.argtypes = [POINTER(genepool_t)]

genepool_calculate_scores = lib.genepool_calculate_scores
genepool_calculate_scores.argtypes = [POINTER(genepool_t), POINTER(map_t), c_size_t]

genepool_avg_score = lib.genepool_avg_score
genepool_avg_score.argtypes = [POINTER(genepool_t)]
genepool_avg_score.restype  = c_double

genepool_max_score = lib.genepool_max_score
genepool_max_score.argtypes = [POINTER(genepool_t)]
genepool_max_score.restype  = c_double

genepools_rebalance = lib.genepools_rebalance
genepools_rebalance.argtypes = [POINTER(genepool_t), c_size_t]

genepool_job_start = lib.genepool_job_start
genepool_job_start.argtypes = [POINTER(genepool_job_t), POINTER(genepool_t), POINTER(map_t), c_size_t]

genepool_job_wait = lib.genepool_job_wait
genepool_job_wait.argtypes = [POINTER(genepool_job_t)]

generateMap = lib.generateMap
generateMap.argtypes = [c_int]
generateMap.restype  = map_t

printMap = lib.printMap
printMap.argtypes = [POINTER(map_t)]
