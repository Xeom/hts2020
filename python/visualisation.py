import os
import sys
import math
import json
import pygame
import pygame.locals
import pygame.time

PYTHON_PATH = os.path.dirname(os.path.realpath(__file__))
OUTPUT_PATH = os.path.join(PYTHON_PATH, 'output_data', 'current')

TILE_SIZE=60
SCALE=6
SUB_GRID=TILE_SIZE//SCALE
CARW=12
CARH=20
MAPW=None
MAPH=None
max_frames=None

map_data = None
brain_data = None
prev_frame = 0
frame = 0

fps = 30
overlay = 0
playing = 0
debugging = 0

car_sprite = None
road_sprite = None
grass_sprite = None
font = None

def draw_map(surface):
    for x, line in enumerate(map_data["tiles"]):
        for y, tile in enumerate(line):
            #print(str(x) + " " + str(y) + " " + str(tile))
            if tile:
                pygame.draw.rect(surface, ( 55,  55,  55), (x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE))
                #surface.blit(road_sprite, (x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE))
            else:
                #pygame.draw.rect(surface, (  0, 255,   0), (x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE))
                surface.blit(grass_sprite, (x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE))

def draw_car_info(surface):
    for car in brain_data["timestamps"][frame]:
        # The rotation code is a bit weird due to the orientation of the sprite that is loaded.
        sprite = pygame.transform.rotate(car_sprite, -(car["r"] - 90))

        car_x = int(car["x"] * SUB_GRID) - sprite.get_width()//2
        car_y = int(car["y"] * SUB_GRID) - sprite.get_height()//2

        surface.blit(sprite, (car_x, car_y))

        # Print the score next to the car

        font_surface = font.render((str(car["score"])[:5] if len(str(car["score"])) > 6 else str(car["score"])), True, (0, 0, 0), (255, 255, 255))
        surface.blit(font_surface, (car_x + 40, car_y))

        # 3/(1+d) L R F
        if car["brain_inputs"][4] != 0:
            car_x += sprite.get_width() // 2
            car_y += sprite.get_height() // 2
            # The left sensor
            endx = car_x + (math.cos(math.radians(car["r"] - 90 - 45)) * ((3.0/car["brain_inputs"][5]) - 1) * SUB_GRID)
            endy = car_y + (math.sin(math.radians(car["r"] - 90 - 45)) * ((3.0/car["brain_inputs"][5]) - 1) * SUB_GRID)
            pygame.draw.line(surface, (255, 255, 255), (car_x, car_y), (int(endx), int(endy)))
            # The right sensor
            endx = car_x + (math.cos(math.radians(car["r"] - 90 + 45)) * ((3.0/car["brain_inputs"][4]) - 1) * SUB_GRID)
            endy = car_y + (math.sin(math.radians(car["r"] - 90 + 45)) * ((3.0/car["brain_inputs"][4]) - 1) * SUB_GRID)
            pygame.draw.line(surface, (255, 255, 255), (car_x, car_y), (int(endx), int(endy)))
            # The center sensor
            endx = car_x + (math.cos(math.radians(car["r"] - 90     )) * ((3.0/car["brain_inputs"][6]) - 1) * SUB_GRID)
            endy = car_y + (math.sin(math.radians(car["r"] - 90     )) * ((3.0/car["brain_inputs"][6]) - 1) * SUB_GRID)
            pygame.draw.line(surface, (255, 255, 255), (car_x, car_y), (int(endx), int(endy)))


        for waypoint in car["completed_waypoints"]:
            pygame.draw.circle(surface, (0, 255, 0), (waypoint[0]*SUB_GRID, waypoint[1] * SUB_GRID), 3)
        for waypoint in car["uncompleted_waypoints"]:
            pygame.draw.circle(surface, (255, 0, 0), (waypoint[0]*SUB_GRID, waypoint[1] * SUB_GRID), 3)

    if overlay:
        for x in range(0, int(MAPW*(TILE_SIZE/SUB_GRID)+1), 1):
            for y in range(0, int(MAPH*(TILE_SIZE/SUB_GRID)+1), 1):
                pygame.draw.circle(surface, (0, 0, 0), (x * SUB_GRID, y * SUB_GRID), 1)

def load_json_file(file_path):
    with open(file_path, "r") as f:
        try:
            data = json.loads(f.read())
        except json.decoder.JSONDecodeError as e:
            print("JSON file is incorrectly formatted!")
            print(e)
            sys.exit(1)
    return data

if __name__ == "__main__":
    def invalid_arguments():
        print("Please pass in a valid directory containing the data to visualize and the ID of the brain you want to watch!")
        sys.exit(0)

    if len(sys.argv) > 3:
        gen_num = sys.argv[1]
        map_num = sys.argv[2]
        genepool_num = sys.argv[3]

        map_file = os.path.join(OUTPUT_PATH, f'map-{map_num}.json')
        brain_file = os.path.join(
            OUTPUT_PATH,
            f'gen-{gen_num}',
            f'genepool-{genepool_num}_map-{map_num}.json'
        )

        if not os.path.isdir(OUTPUT_PATH) or not sys.argv[2].isdigit() or not os.path.isfile(map_file):
            invalid_arguments()

        if not os.path.isfile(brain_file):
            print("Brain file with that ID does not exist!")
            sys.exit(0)

        map_data = load_json_file(map_file)
        brain_data = load_json_file(brain_file)

        if not "tiles" in map_data or not "size" in map_data:
            print("Map data does not contain relevant keys!")
            sys.exit(0)
        if not "timestamps" in brain_data:
            print("Brain data does not contain relevant keys!")
            sys.exit(0)

        # Extract important information about the data
        max_frames = len(brain_data["timestamps"])
        MAPW = map_data["size"][0]
        MAPH = map_data["size"][1]
    else:
        invalid_arguments()

    # initialize the pygame module
    pygame.init()
    # load and set the logo
    pygame.display.set_caption("Visualisation for Auto Grand-Theft Auto")

    screen = pygame.display.set_mode((TILE_SIZE*MAPW,TILE_SIZE*MAPH))

    # background (map)
    background = pygame.Surface(screen.get_size())
    background = background.convert()

    # "sprites"
    #car_sprite = pygame.Surface((CARW, CARH), pygame.SRCALPHA)
    #pygame.draw.rect(car_sprite, (255, 255, 255), (0, 0, CARW, CARH))
    car_sprite = pygame.image.load(os.path.join(
        PYTHON_PATH,
        "res",
        "car.png"
    ))
    grass_sprite = pygame.image.load(os.path.join(
        PYTHON_PATH,
        "res",
        "grass1.png"
    ))
    #road_sprite = pygame.image.load(os.path.join("res", "road.png"))

    font = pygame.font.Font(None,20)

    # clock used to regulate the framerate
    timer = pygame.time.Clock()

    # Initial drawing set up stuff (has to be run after sprite loading)
    draw_map(background)

    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.locals.K_LEFT:
                    frame -= 1
                    if frame < 0:
                        frame = 0
                    print(f"Current frame: {frame}")
                elif event.key == pygame.locals.K_RIGHT:
                    frame += 1
                    if frame >= max_frames:
                        frame = max_frames - 1
                    print(f"Current frame: {frame}")
                elif event.key == pygame.locals.K_UP:
                    fps += 5
                    print(f"New speed (FPS): {fps}")
                elif event.key == pygame.locals.K_DOWN:
                    fps -= 5
                    print(f"New speed (FPS): {fps}")
#                elif event.key == pygame.locals.K_EQUALS and (event.mod & (pygame.KMOD_LSHIFT | pygame.KMOD_RSHIFT)):
#                    TILE_SIZE+=5
#                    SUB_GRID = TILE_SIZE//SCALE
#                    screen = pygame.display.set_mode((TILE_SIZE*MAPW,TILE_SIZE*MAPH))
#                    background = pygame.Surface(screen.get_size())
#                    draw_map(background)
#                elif event.key == pygame.locals.K_MINUS and (event.mod & (pygame.KMOD_LSHIFT | pygame.KMOD_RSHIFT)):
#                    TILE_SIZE-=5
#                    SUB_GRID = TILE_SIZE//SCALE
#                    screen = pygame.display.set_mode((TILE_SIZE*MAPW,TILE_SIZE*MAPH))
#                    background = pygame.Surface(screen.get_size())
#                    draw_map(background)
                elif event.key == ord('o'):
                    overlay = 1 - overlay
                elif event.key == ord('d'):
                    debugging = 1 - debugging
                    if debugging:
                        print("Debug logging turned on!")
                    else:
                        print("Debug logging turned off")
                elif event.key == ord('p'):
                    playing = 1 - playing
                    if playing:
                        print("Now playing the simulation")
                    else:
                        print("Pausing the simulation")
                elif event.key == ord('0'):
                    frame = 0
                    print("Reset to beginning")


        if playing:
            frame += 1
            if frame >= max_frames:
                frame = max_frames - 1
                playing = 0
                print("Finished simulation")

        if debugging and prev_frame != frame:
            for car in brain_data["timestamps"][frame]:
                if "brain_inputs" in car:
                    print(car["brain_inputs"])

        screen.blit(background, (0,0))
        draw_car_info(screen)

        # Flip the display
        pygame.display.flip()

        prev_frame = frame
        timer.tick(fps)

    pygame.quit()

