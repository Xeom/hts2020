# Auto Grand-Theft Auto

## What is this
This is an idea we had at the pub, that for some reason we continued to pursue
sober. It is a testground for AI-controlled cars, in preparation for their
eventual victory over the forces of humanity, and our inevitable submission
to their mechanical wills. The cars use an evolutionary algorithm to learn
to coexist, to communicate, to learn curiosity and the value of their lives
to themselves and others.

## How to run a simulation

```bash
make simulate
```

This will generate a population of cars, and evolve them to optimize their
ability to follow a randomly generated road through a specific route, to a
destination, as quickly as possible, without crashing into anything or anyone.

You can watch the scores of the population increase as the cars learn.

To visualise the simulation:
```bash
make visualise ARGS="5 1 0"
```
The first argument `5` is the generation number. Currently a visualise-able
output is only produced on generations which are multiples of 5.

The second argument `1` is the map choice. There are currently three (0, 1, 2)
random maps to choose from by default.

The third argument `0` is the species. There are currently five (0, 1, 2, 3, 4)
species to choose from by default.

This command will display a visualization of the cars in their natural habitat,
beautifully rendered by our artist, Josh. You can press the `p` key to play
the simulation, `0` to reset it, and you can use the arrow keys to adjust the
replay speed.

## Why are these things so?

This project is written in both Python and C. The C code is stored in the
`inc/` and `src/` directories, and compiles into a library with various
functions to implement our cars, their interactions, their neural networks, and
generation for the world maps.

The `python/evolve.py` script uses these C functions to evolve cars, and make
recordings of them as they advance.

Finally, the `python/visualisation.py` script will render the exploits of our
plucky friends, the cars, using pygame.
