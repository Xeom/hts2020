#include"../inc/map.h"

void tileToCoord(tile_t tile, coord_t coord)
{
    coord[0] = TILE_SIZE*tile[0] + TILE_SIZE/2;
    coord[1] = TILE_SIZE*tile[1] + TILE_SIZE/2;
}

bool routeStepRight(route_t route, size_t* step)
{
    bool end_of_route;

    int dir_side_steps = rand() % 2;
    int num_side_steps = rand() % 6;

    int next_y, next_x;

    if (num_side_steps > 3) num_side_steps = 0;

    next_x = route[*step -1][0] +1;
    next_y = route[*step -1][1];

    if (next_x == MAP_WIDTH -1) end_of_route = true;
    else end_of_route = false;

    route[*step][0] = next_x;
    route[*step][1] = next_y;

    for (size_t side_step = 0; side_step < num_side_steps; ++side_step)
    {
            *step += 1;

            if (dir_side_steps == 0) next_y += 1;
            else next_y -= 1;

            if (
                (next_y < 0) ||
                (next_y > (MAP_HEIGHT -1))
            ) {
                *step -= 1;
                break;
            }
            else {
                route[*step][0] = next_x;
                route[*step][1] = next_y;
            }
    }
    return end_of_route;
}

bool routeStepLeft(route_t route, size_t* step)
{
    bool end_of_route;

    int dir_side_steps = rand() % 2;
    int num_side_steps = rand() % WOBBLE_0;

    int next_y, next_x;

    if (num_side_steps > WOBBLE_1) num_side_steps = 0;

    next_x = route[*step -1][0] -1;
    next_y = route[*step -1][1];

    if (next_x == 0) end_of_route = true;
    else end_of_route = false;

    route[*step][0] = next_x;
    route[*step][1] = next_y;

    for (size_t side_step = 0; side_step < num_side_steps; ++side_step)
    {
            *step += 1;

            if (dir_side_steps == 0) next_y += 1;
            else next_y -= 1;

            if (
                (next_y < 0) ||
                (next_y > (MAP_HEIGHT -1))
            ) {
                *step -= 1;
                break;
            }
            else {
                route[*step][0] = next_x;
                route[*step][1] = next_y;
            }
    }
    return end_of_route;
}

bool routeStepDown(route_t route, size_t* step)
{
    bool end_of_route;

    int dir_side_steps = rand() % 2;
    int num_side_steps = rand() % 6;

    int next_y, next_x;

    if (num_side_steps > 3) num_side_steps = 0;

    next_x = route[*step -1][0];
    next_y = route[*step -1][1] +1;

    if (next_y == MAP_HEIGHT -1) end_of_route = true;
    else end_of_route = false;

    route[*step][0] = next_x;
    route[*step][1] = next_y;

    for (size_t side_step = 0; side_step < num_side_steps; ++side_step)
    {
            *step += 1;

            if (dir_side_steps == 0) next_x += 1;
            else next_x -= 1;

            if (
                (next_x < 0) ||
                (next_x > (MAP_WIDTH -1))
            ) {
                *step -= 1;
                break;
            }
            else {
                route[*step][0] = next_x;
                route[*step][1] = next_y;
            }
    }
    return end_of_route;
}

bool routeStepUp(route_t route, size_t* step)
{
    bool end_of_route;

    int dir_side_steps = rand() % 2;
    int num_side_steps = rand() % 6;

    int next_y, next_x;

    if (num_side_steps > 3) num_side_steps = 0;

    next_x = route[*step -1][0];
    next_y = route[*step -1][1] -1;

    if (next_y == 0) end_of_route = true;
    else end_of_route = false;

    route[*step][0] = next_x;
    route[*step][1] = next_y;

    for (size_t side_step = 0; side_step < num_side_steps; ++side_step)
    {
            *step += 1;

            if (dir_side_steps == 0) next_x += 1;
            else next_x -= 1;

            if (
                (next_x < 0) ||
                (next_x > (MAP_WIDTH -1))
            ) {
                *step -= 1;
                break;
            }
            else {
                route[*step][0] = next_x;
                route[*step][1] = next_y;
            }
    }
    return end_of_route;
}

void makeRoute(
    tile_t start_tile,
    car_dir_t direction,
    route_t route,
    waypoints_t waypoints,
    int* num_steps,
    int* numwaypoints
) {
    size_t step;
    bool end_of_route = false;

    route[0][0] = start_tile[0];
    route[0][1] = start_tile[1];

    tileToCoord(route[0], waypoints[0]);
    *numwaypoints = 1;

    for (step = 1; step < ROUTE_MAX_STEPS; ++step)
    {
        switch(direction)
        {
        case DIR_RIGHT:
            end_of_route = routeStepRight(route, &step);
            break;

        case DIR_LEFT:
            end_of_route = routeStepLeft(route, &step);
            break;

        case DIR_DOWN:
            end_of_route = routeStepDown(route, &step);
            break;

        case DIR_UP:
            end_of_route = routeStepUp(route, &step);
            break;
        }

        *numwaypoints += 1;
        tileToCoord(route[step], waypoints[*numwaypoints -1]);
        if (end_of_route) break;
    }
    *num_steps = step +1;
    return;
}

bool tileTaken(
    tile_t      tile_to_check,
    cars_tile_t other_tiles,
    int         check_till // index to stop before
) {
    for (size_t ind = 0; ind < check_till; ++ind)
    {
        if ((tile_to_check[0] == other_tiles[ind][0]) &&
            (tile_to_check[1] == other_tiles[ind][1]))
        {
            return true;
        }
    }
    return false;
}

void generateStartTiles(
    int         num_cars,
    cars_tile_t start_tiles,
    car_dir_t direction[MAX_CARS]
) {
    size_t ind;

    // first car doesn't need to check if a position is taken
    start_tiles[0][0] = 0;
    start_tiles[0][1] = rand() % MAP_HEIGHT;
    direction[0] = DIR_RIGHT;

    for (ind = 1; ind < num_cars; ++ind)
    {
        direction[ind] = (direction[ind -1] +1) %4;

        do
        {
            switch(direction[ind])
            {
            case DIR_RIGHT:// left to right
                start_tiles[ind][0] = 0;
                start_tiles[ind][1] = rand() % MAP_HEIGHT;
                break;

            case DIR_DOWN:// top to bottom
                start_tiles[ind][0] = rand() % MAP_WIDTH;
                start_tiles[ind][1] = 0;
                break;

            case DIR_LEFT:// right to left
                start_tiles[ind][0] = MAP_WIDTH -1;
                start_tiles[ind][1] = rand() % MAP_HEIGHT;
                break;

            case DIR_UP:// bottom to top
                start_tiles[ind][0] = rand() % MAP_WIDTH;
                start_tiles[ind][1] = MAP_HEIGHT - 1;
                break;
            }
        } while (tileTaken(start_tiles[ind], start_tiles, ind));
    }
}

void printRoute(route_t route, int num_steps)
{
    bool bitmap[MAP_WIDTH][MAP_HEIGHT];
    memset(bitmap, 0, sizeof(bitmap));

    int x, y;

    for (size_t step = 0; step < num_steps; ++step)
    {
        x = route[step][0];
        y = route[step][1];

        bitmap[x][y] = true;
    }
    for (size_t j = 0; j < MAP_HEIGHT; ++j)
    {
        for (size_t i = 0; i < MAP_WIDTH; ++i)
        {
            printf(" %d", bitmap[i][j]);
        }
        printf("\n");
    }
}

void buildRoads(
    route_t routes[MAX_CARS],
    int num_steps[MAX_CARS],
    int num_cars,
    bool road[MAP_WIDTH][MAP_HEIGHT]
) {
    memset(road, 0, sizeof(road));
    int x, y;

    for (size_t ind = 0; ind < num_cars; ++ind)
    {
        for (size_t step = 0; step < num_steps[ind]; ++step)
        {
            x = routes[ind][step][0];
            y = routes[ind][step][1];

            road[x][y] = true;
        }
    }
}

void printRoads(bool road[MAP_WIDTH][MAP_HEIGHT])
{
    for (size_t j = 0; j < MAP_HEIGHT; ++j)
    {
        for (size_t i = 0; i < MAP_WIDTH; ++i)
        {
            printf(" %d", road[i][j]);
        }
        printf("\n");
    }
}

map_t generateMap(int num_cars)
{
    map_t map;
    map.num_cars = num_cars;

    cars_tile_t start_tiles;

    int     num_steps[MAX_CARS];
    route_t routes[MAX_CARS];

    car_dir_t directions[MAX_CARS];

    if (map.num_cars < 1)
        printf("ERROR: Need at least one car to generate a map.\n");
    if (map.num_cars > MAX_CARS)
        printf("ERROR: Can't have more than MAX_CARS.\n");

    generateStartTiles(map.num_cars, start_tiles, directions);

    for (size_t ind = 0; ind < map.num_cars; ++ind)
    {
        // makes the car's route
        makeRoute(
            start_tiles[ind],
            directions[ind],
            routes[ind],
            map.waypoints[ind],
            &num_steps[ind],
            &map.numwaypoints[ind]
        );

        // sets the car's starting position
        tileToCoord(start_tiles[ind], map.start_pos[ind]);

        // sets the car's starting direction
        switch(directions[ind])
        {
        case DIR_UP:
            map.start_dir[ind] = 0;
            break;
        case DIR_RIGHT:
            map.start_dir[ind] = 90;
            break;
        case DIR_DOWN:
            map.start_dir[ind] = 180;
            break;
        case DIR_LEFT:
            map.start_dir[ind] = 270;
            break;
        }
    }
    buildRoads(routes, num_steps, map.num_cars, map.road);
    return map;
}

void printMap(map_t* map)
{
    for (int car = 0; car < map->num_cars; ++car)
    {
        printf(
            "car %d starts at (%f, %f) and %f degrees\n",
            car,
            map->start_pos[car][0],
            map->start_pos[car][1],
            map->start_dir[car]
        );
    }
    printRoads(map->road);
}

/*
int main()
{
    map_t map;
    map = generateMap();
    printMap(&map);
}
*/
