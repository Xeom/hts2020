#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "simulate.h"

static double car_angle_to_waypoint(car_t *car);
static double car_dist_to_grass(car_t *car, double angle, map_t *map);
static void car_tile(car_t *car, int *res);
static void car_collide(car_t *car, car_t *other);
void simulation_init(simulation_t *sim, map_t *map, brain_t *brain)
{
    size_t ind;

    memset(sim, 0, sizeof(simulation_t));
    sim->map = map;

    for (ind = 0; ind < (size_t)map->num_cars; ++ind)
    {
        car_t *car;
        size_t wpind;

        car = &(sim->cars[ind]);
        car_init(car, brain);
        car->x = map->start_pos[ind][0];
        car->y = map->start_pos[ind][1];
        car->dir = map->start_dir[ind];
        car->id  = ind;
        car->steps = 0;
        car->numwaypoints = map->numwaypoints[ind];

        for (wpind = 0; wpind < MAX_WAYPOINTS; ++wpind)
        {
            car->waypoints[wpind][0] = map->waypoints[ind][wpind][0];
            car->waypoints[wpind][1] = map->waypoints[ind][wpind][1];
        }
    }
}

void car_init(car_t *car, brain_t *brain)
{
    memset(car, 0, sizeof(car_t));
    car->extant = true;
    brain_copy(&(car->brain), brain);
}

void simulation_step(simulation_t *sim)
{
    size_t ind;

    if (sim->ended)
        return;

    for (ind = 0; ind < MAX_CARS; ++ind)
    {
        car_think(&(sim->cars[ind]), sim);
    }

    for (ind = 0; ind < MAX_CARS; ++ind)
    {
        if (!sim->cars[ind].extant) continue;
        car_drive(&(sim->cars[ind]), sim);

        sim->cars[ind].rx = 0;
        // check for collisions with other cars
        for (size_t other = 0; other < MAX_CARS; ++other)
        {
            if (other == ind) continue;
            if (!sim->cars[other].extant) continue;
            car_collide(&(sim->cars[ind]), &(sim->cars[other]));
        }

        if (sim->cars[ind].crashed)
            sim->ended = true;
    }
}

double simulation_score(simulation_t *sim)
{
    double score;
    size_t ind;

    score = 0;
    for (ind = 0; ind < MAX_CARS; ++ind)
    {
        score += car_score(&(sim->cars[ind]));
    }
    return score;
}

void car_think(car_t *car, simulation_t *sim)
{
    brain_t *brain;

    if (!car->extant)
        return;

    brain = &(car->brain);

    brain_clear_calculated(brain);

    brain->nodes[NODE_CONST_1].val = 1;
    brain->nodes[NODE_RAND].val = rand() % 1;
    brain->nodes[NODE_ID].val = car->id / MAX_CARS;
    brain->nodes[NODE_GRASS_LEFT] .val = 3.0/(1.0 + car_dist_to_grass(car, +45.0, sim->map));
    brain->nodes[NODE_GRASS_RIGHT].val = 3.0/(1.0 + car_dist_to_grass(car, -45.0, sim->map));
    brain->nodes[NODE_GRASS_FRONT].val = 3.0/(1.0 + car_dist_to_grass(car, 0.0,   sim->map));
    brain->nodes[NODE_WAYPOINT_ANGLE].val = car_angle_to_waypoint(car) / 180.0;
    brain->nodes[NODE_RX].val = car->rx;

    brain_calculate_outputs(brain);
    car->tx = brain->nodes[NODE_RX].val;
}

static void car_tile(car_t *car, int *res)
{
    res[0] = floor(car->x / TILE_SIZE);
    res[1] = floor(car->y / TILE_SIZE);
    if (car->x < 0) res[0] = -1;
    if (car->y < 0) res[1] = -1;
}

double car_score(car_t *car)
{
    size_t ind;
    double score;
    double completeddist;
    completeddist = 0.0;

    score = 0;

    size_t sumupto;
    if (car->waypointind >= 0)
    {
        sumupto = car->waypointind;
    }
    else
    {
        sumupto = car->numwaypoints;
    }

    for (ind = 0; ind < sumupto; ++ind)
    {
        completeddist += sqrt(
            pow(car->waypoints[ind][0] - car->waypoints[ind + 1][0], 2) +
            pow(car->waypoints[ind][1] - car->waypoints[ind + 1][1], 2)
        );
    }

    if (car->waypointind >= 0)
    {
        completeddist -= sqrt(
            pow(car->x - car->waypoints[car->waypointind][0], 2) +
            pow(car->y - car->waypoints[car->waypointind][1], 2)
        );
    }
    else
    {
        score += 1000;
    }

    score += completeddist * (20 + completeddist/(1 + car->steps));

    return score;
}

static void car_collide(car_t *car, car_t *other)
{
    // Using circles because it turns out collision detection between rotated rectangles is a PITA
    double radius = CAR_HEIGHT;

    double dx = car->x - other->x;
    double dy = car->y - other->y;

    double d = sqrt(pow(dx, 2) + pow(dy, 2));

    if (d < radius * 2)
    {
        car->crashed = true;
        other->crashed = true;
    }
    car->rx += 5 * other->tx / (1 + d * d);
}

static double car_dist_to_grass(car_t *car, double angle, map_t *map)
{
    double dist;
    double dxdd, dydd;

    const double maxdist = 30;
    const double step    = 0.1;

    angle = car->dir + angle;

    dydd = -cos(angle / 180.0 * 3.14159);
    dxdd =  sin(angle / 180.0 * 3.14159);

    dist = 0.0;

    while (dist < maxdist)
    {
        int xcoord, ycoord;
        xcoord = floor((car->x + (dist * dxdd)) / TILE_SIZE);
        ycoord = floor((car->y + (dist * dydd)) / TILE_SIZE);

        if (xcoord < 0 || ycoord < 0 ||
            xcoord >= MAP_WIDTH || ycoord >= MAP_HEIGHT ||
            !map->road[xcoord][ycoord])
            return dist;

        dist += step; 
    }

    return dist;
}

static double car_angle_to_waypoint(car_t *car)
{
    double dx, dy, angle;
    if (car->waypointind < 0)
        return 0.0;

    dx = (double)car->waypoints[car->waypointind][0] - car->x;
    dy = (double)car->waypoints[car->waypointind][1] - car->y;

    angle = atan2(dy, dx) * 180 / 3.14159;
    angle -= car->dir;
    angle += 90.0;

    while (angle > +180.0) angle -= 360.0;
    while (angle < -180.0) angle += 360.0;

    return angle;
}

void car_drive(car_t *car, simulation_t *sim)
{
    int tile[2];

    if (!car->extant)
        return;

    if (car->crashed)
        return;

    if (car->waypointind < 0)
        return;

    car->steps += 1;

    car_tile(car, tile);

    if (car->waypointind >= car->numwaypoints)
    {
        car->waypointind = -1;
    }

    if (
        car->waypointind >= 0 &&
        tile[0] == car->waypoints[car->waypointind][0]/TILE_SIZE &&
        tile[1] == car->waypoints[car->waypointind][1]/TILE_SIZE)
    {
        car->waypointind += 1;
        if (car->waypointind >= car->numwaypoints)
        {
            car->waypointind = -1;
        }
    }

    if (tile[0] <  0         || tile[1] <  0 ||
        tile[0] >= MAP_WIDTH || tile[1] >= MAP_HEIGHT ||
        !sim->map->road[tile[0]][tile[1]])
    {
        car->crashed = true;
        return;
    }

    double moverate = 0.5;
    double turnrate = 15.0;

    double velocity, turnspeed;

    velocity = moverate * car->brain.nodes[NODE_DRIVE].val;
    if (velocity > moverate || velocity < -moverate * 0.5)
        velocity = moverate;

    turnspeed = turnrate * car->brain.nodes[NODE_TURN].val;
    if (turnspeed > turnrate || turnspeed < -turnrate)
        turnspeed = turnrate;

    car->dir += turnspeed;
    car->y   += -cos(car->dir / 180 * 3.14159) * velocity;
    car->x   +=  sin(car->dir / 180 * 3.14159) * velocity;
}
