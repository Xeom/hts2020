#include "genepool.h"
#include <string.h>
#include <pthread.h>

void genepool_interbreed(genepool_t *gp)
{
    int ind;

    for (ind = 0; ind < gp->size; ++ind)
    {
        brain_copy(&gp->members[ind], &gp->best);
        brain_mutate(&gp->members[ind]);
    }
}

static void genepool_calculate_scores_on_map(genepool_t *gp, map_t *map)
{
    int ind;
    simulation_t sim;

    for (ind = 0; ind < gp->size; ++ind)
    {
        brain_t *brain;
        size_t nsteps = 500;
        brain = &gp->members[ind];
        simulation_init(&sim, map, brain);
        while (nsteps-- > 0)
            simulation_step(&sim);

        gp->totalscore[ind] += simulation_score(&sim);
    }
}

void genepool_calculate_scores(genepool_t *gp, map_t *maps, size_t nmaps)
{
    int ind;

    memset(&(gp->totalscore), 0, sizeof(gp->totalscore));

    for (ind = 0; ind < (int)nmaps; ++ind)
    {
        genepool_calculate_scores_on_map(gp, &maps[ind]);
    }
}

void genepool_choose_best(genepool_t *gp)
{
    int ind;

    gp->bestscore = -100000.0;
    for (ind = 0; ind < gp->size; ++ind)
    {
        if (gp->totalscore[ind] > gp->bestscore)
        {
            memcpy(&gp->best, &gp->members[ind], sizeof(brain_t));
            gp->bestscore = gp->totalscore[ind];
        }
    }
}

void genepool_split(genepool_t *gp, genepool_t *oth)
{
    gp->size  += oth->size;
    gp->size  -= gp->size / 2;
    oth->size += gp->size / 2;

    memcpy(&(oth->best), &oth->members[0], sizeof(brain_t));
}

double genepool_avg_score(genepool_t *gp)
{
    double sum;
    int ind;

    for (ind = 0; ind < gp->size; ++ind)
    {
        sum += gp->totalscore[ind];
    }

    return sum / gp->size;
}

double genepool_max_score(genepool_t *gp)
{
    return gp->bestscore;
}

void genepools_rebalance(genepool_t *gpvec, size_t n)
{
    double pkillmax = 0.1;
    double pkillavg = 0.1;

    int minmaxind = 0;
    int minavgind = 0;

    double minmaxval = genepool_max_score(gpvec);
    double minavgval = genepool_avg_score(gpvec);

    int ind;

    for (ind = 0; ind < (int)n; ++ind)
    {
        if (genepool_avg_score(&gpvec[ind]) < minavgval)
        {
            minavgind = ind;
            minavgval = genepool_avg_score(&gpvec[ind]);
        }

        if (genepool_max_score(&gpvec[ind]) < minmaxval)
        {
            minmaxind = ind;
            minmaxval = genepool_max_score(&gpvec[ind]);
        }
    }

    if (rand_float() < pkillmax)
    {
        printf("Killed genepool %d due to low max of %f\n", minmaxind, minmaxval);
        brain_copy(&gpvec[minmaxind].best, &gpvec[rand() % n].best);
    }

    if (rand_float() < pkillavg)
    {
        printf("Killed genepool %d due to low avg of %f\n", minavgind, minavgval);
        brain_copy(&gpvec[minavgind].best, &gpvec[rand() % n].best);
    }
}

static void *genepool_job_thread(void *arg)
{
    genepool_job_t *gp;
    gp = arg;
    genepool_interbreed(gp->gp);
    genepool_calculate_scores(gp->gp, gp->maps, gp->nmaps);
    genepool_choose_best(gp->gp);

    return NULL;
}

void genepool_job_start(genepool_job_t *job, genepool_t *gp, map_t *maps, size_t nmaps)
{
    job->gp = gp;
    job->maps = maps;
    job->nmaps = nmaps;
    pthread_create(&(job->thread), NULL, genepool_job_thread, job);
}

void genepool_job_wait(genepool_job_t *job)
{
    pthread_join(job->thread, NULL);
}


