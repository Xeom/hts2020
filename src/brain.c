#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "brain.h"
#include "simulate.h"
#include <time.h>

void brain_init(brain_t *brain)
{
    size_t nodeind, linkind;
    memset(brain, 0, sizeof(brain_t));

    for (nodeind = 0; nodeind < MAX_NODES; ++nodeind)
    {
        node_t *node;
        node = &brain->nodes[nodeind];

        if (nodeind <= NODE_LAST_INPUT)
        {
            node->extant = true;
            node->input  = true;
        }
        else if (nodeind <= NODE_LAST_HIDDEN && nodeind >= NODE_FIRST_HIDDEN)
        {
            node->extant = true;
            for (linkind = 0; linkind < MAX_INPUTS; ++linkind)
            {
                neuron_t *neuron;
                neuron = &node->links[linkind];

                if (linkind <= NODE_LAST_INPUT)
                {
                    neuron->extant  = true;
                    neuron->val     = rand_float() - 0.5;
                    neuron->inpnode = linkind;
                }
            }
        }
        else if (nodeind <= NODE_LAST_OUTPUT && nodeind >= NODE_FIRST_OUTPUT)
        {
            node->extant = true;
            node->output  = true;
            for (linkind = 0; linkind < MAX_INPUTS; ++linkind)
            {
                neuron_t *neuron;
                neuron = &node->links[linkind];

                if (linkind <= (NODE_LAST_HIDDEN - NODE_FIRST_HIDDEN))
                {
                    neuron->extant  = true;
                    neuron->val     = rand_float() - 0.5;
                    neuron->inpnode = linkind + NODE_FIRST_HIDDEN;
                }
            }
            
        }
    }   
}

void brain_copy(brain_t *brain, brain_t *oth)
{
    memcpy(brain, oth, sizeof(brain_t));
}

void brain_clear_calculated(brain_t *brain)
{
    size_t ind;
    for (ind = 0; ind < MAX_NODES; ++ind)
    {
        if (brain->nodes[ind].extant)
        {
            brain->nodes[ind].calculated = false;
        }
    }
}

void brain_calculate_node(brain_t *brain, node_t *node)
{
    size_t ind;
    double sum;
    int    numlinks;

    if (node->input)
    {
        return;
    }

    if (node->calculated)
    {
        return;
    }

    sum = 0;
    for (ind = 0; ind < MAX_INPUTS; ++ind)
    {
        neuron_t *link;
        link = &node->links[ind];

        if (link->extant)
        {
            node_t *inpnode;
            numlinks += 1;

            inpnode = &brain->nodes[link->inpnode];
            brain_calculate_node(brain, inpnode);


            sum += inpnode->val * link->val;
        }
    }

    node->val = sum;
}

void brain_calculate_outputs(brain_t *brain)
{
    size_t ind;

    for (ind = 0; ind < MAX_NODES; ++ind)
    {
        node_t *output;
        output = &brain->nodes[ind];

        if (output->extant && output->output)
        {
            brain_calculate_node(brain, output);
        }
    }
}

void brain_mutate(brain_t *brain)
{

    size_t nodeind, linkind;
    const float pmutate    = 0.1;
    const float amplmutate = 0.2;

    for (nodeind = 0; nodeind < MAX_NODES; ++nodeind)
    {
        node_t *node;
        node = &brain->nodes[nodeind];

        for (linkind = 0; linkind < MAX_INPUTS; ++linkind)
        {
            neuron_t *neuron;
            neuron = &node->links[linkind];

            if (rand_float() < pmutate)
            {
                neuron->val += amplmutate * 2 * (rand_float() - 0.5);
            }
        }
    }
}