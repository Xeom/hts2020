CC = gcc
CFLAGS = -g -Wextra -Wmissing-prototypes -Wno-unused-parameter -Wno-switch -Wno-attributes -Wall -Iinc -pthread

OBJDIR = obj
INCDIR = inc
SRCDIR = src
BINDIR = bin

HFILES=$(wildcard $(INCDIR)/*.h)
OFILES=$(subst .c,.o,$(subst $(SRCDIR),$(OBJDIR),$(wildcard $(SRCDIR)/*.c)))

$(OBJDIR)/%.o: $(SRCDIR)/%.c $(HFILES)
	mkdir -p $(@D)
	$(CC) -c $(CFLAGS) $< -o $@

$(BINDIR)/autogta.so: $(OFILES)
	mkdir -p $(@D)
	$(CC) $(CFLAGS) -shared $^ -o $@

simulate: all
	python3 python/evolve.py

visualise: all
	python3 python/visualisation.py $(ARGS)

clean:
	rm -f *.o

all: $(BINDIR)/autogta.so

.PHONEY=all clean simulate visualise
.DEFAULT_GOAL=all
